from collections import Counter

import jsonlines
from pymongo import MongoClient

name = '' # user here

# Get information from database
client = MongoClient()
db = client.instagram
collection = db.instagram_initial
item = collection.find_one({'_id': name})

# Define variables to contain data
pictures_checked = list()
picture_unfinished = list()
likers = list()
commenters = list()

for post in item['posts']:
    if post['date'] not in pictures_checked:
        pictures_checked.append(post['date'])
        for liker in post['liked_by']:
            likers.append(liker)
        for commenter in post['comments']:
            commenter_name = commenter[0]
            commenters.append(commenter_name)

        # urls for further iterations
        likes = int(str(post.get('like_count', 0)).split('.')[0])
        liked_by = len(post['liked_by'])
        if likes != liked_by and not post['video_content']:
            picture_unfinished.append(post['url'])

filename = name + '-stat.txt'
with open(filename, 'w') as file:
    file.write('Likes:\n')
    for liker_tuple in sorted(Counter(likers).items(), key=(lambda x: x[1]), reverse=True):
        name = str(liker_tuple[0]).split('/')[1]
        likes = str(liker_tuple[1])
        file.write(name + ' - ' + likes + '\n')

    file.write('\n---------------\n\nComments:\n')

    for comment_tuple in sorted(Counter(commenters).items(), key=(lambda x: x[1]), reverse=True):
        name = str(comment_tuple[0])
        comment = str(comment_tuple[1])
        file.write(name + ' - ' + comment  + '\n')

    file.write('\n---------------\n\n')
    file.write('Total likes: ' + str(len(likers)) + '\n')
    file.write('Total comments: ' + str(len(commenters)) + '\n')
    file.write('Pictures checked: ' + str(len(pictures_checked)) + ' from ' + str(len(item['posts'])) + '\n')

with open('urls_unfinised.txt', 'w') as file:
    for urls in picture_unfinished:
        file.write(urls + '\n')

sum = 0
for i in range(1,10):
    sum += i

print(sum*2-10)
