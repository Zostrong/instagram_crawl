"""
Script for creating the meta data next to the scrapped pictures

RUN AFTER SCRAPY FINISHED
"""


import json
from pymongo import MongoClient
# import jsonlines

# with jsonlines.open('result.jl') as reader:
#     for item in reader.iter(type=dict, skip_invalid=True):
#         json_filename = 'pictures/%s/%s.json' % (str(item['name']), str(item['posts']['date']).replace(':', '-'))
#         with open(json_filename, 'w', encoding='utf8') as json_writer:
#             json.dump(dict(item), json_writer, indent=4, ensure_ascii=False)

name = '' # user here

client = MongoClient()
db = client.instagram
collection = db.instagram_initial

item = collection.find_one({'_id': name})
for post in item['posts']:
    json_filename = 'pictures/%s/%s.json' % (str(item['_id']), str(post['date']).replace(':', '-'))
    with open(json_filename, 'w', encoding='utf8') as json_writer:
        post_info = {
            'name': item['_id'],
            'post': post
        }
        json.dump(post_info, json_writer, indent=4, ensure_ascii=False)
