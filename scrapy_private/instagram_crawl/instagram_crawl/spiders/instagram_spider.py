import scrapy
import pymongo as pymongo
from scrapy.exceptions import CloseSpider
from instagram_crawl.items import InstagramItem

# TODO: max picture take in consideration when not iterating
class InstagramSpider(scrapy.Spider):
    name = 'instagram-stat'
    person = ''
    hashtag = ''
    max_pic_on_profile = 0
    picture_count = 0
    max_pictures = 0
    picture_urls = set()
    has_item_been_scrapped = False

    def __init__(self, *args, **kwargs):
        self.iterate = getattr(self, 'iterate', False)
        self.max = getattr(self, 'max', 12)
        super(InstagramSpider, self).__init__(*args, **kwargs)

    def start_requests(self):
        urls = list()

        try:
            self.max_pictures = int(getattr(self, 'max', 12))
        except Exception:
            raise CloseSpider(reason="Max value has to be a number")

        self.person = getattr(self, 'person', '')

        item = InstagramItem()
        item['_id'] = self.person
        item['posts'] = list()
        item['unfinished_urls'] = list()

        if self.iterate:
            collection_name = 'instagram_initial'
            mongo_uri = self.settings.get('MONGO_URI'),
            mongo_db = self.settings.get('MONGO_DATABASE')

            client = pymongo.MongoClient(mongo_uri)
            db = client[mongo_db]
            person_data = db[collection_name].find_one({'_id': self.person})
            for url in person_data['unfinished_urls']:
                urls.append(str(url))

            # with open('urls_unfinised.txt', 'r') as file:
            #     urls = file.read().splitlines()
        else:
            if self.person == '':
                raise CloseSpider(reason='User has to be given')
            else:
                urls.append('https://www.instagram.com/' + str(self.person) + '/')

        for url in urls:
            if self.picture_count < self.max_pictures and self.iterate:
                self.picture_count += 1
                yield scrapy.Request(url, self.parse_post, meta={'item': item})
            elif not self.iterate:
                for i in range(self.max_pictures // 12):
                    yield scrapy.Request(url, self.parse_main, meta={'item': item, 'new': 1}, dont_filter=True)
            else:
                break

    def parse_main(self, response):
        item = response.meta['item']
        for href in response.xpath('//div[@class="_havey"]//a/@href').extract():
            self.picture_urls.add('https://instagram.com' + href)
        if self.max_pic_on_profile is 0:
            self.max_pic_on_profile = int(response.xpath('//ul[@class="_h9luf"]/li[1]/span/span/text()').extract_first())

        if len(self.picture_urls) == self.max_pic_on_profile \
                and self.max_pic_on_profile is not 0 and not self.has_item_been_scrapped:
            self.has_item_been_scrapped = True
            for pic_url in self.picture_urls:
                yield scrapy.Request(pic_url, self.parse_post, meta={'item': item})

    def parse_post(self, response):
        # Instagram Item
        item = response.meta['item']

        # Information of the image
        item['image_urls'] = []
        item['file_urls'] = []
        image_tags = response.xpath('//a[contains(@class, "_n1lhu") and contains(@class, "_4dsc8")]/@href').extract()
        image_content = response.xpath('//div[@class="_4rbun"]/img/@src').extract()
        video_content = response.xpath('//div[@class="_qzesf"]/video/@src').extract()

        item['image_urls'] = image_content
        item['file_urls'] = video_content

        # Likes
        if image_content:
            like_count = response.xpath('//a[contains(@class, "_nzn1h")]/span/text()').extract_first()
            if like_count is not None:
                liked_by = response.xpath('//div[@class="_2nunc"]/a/@href').extract()
            else:
                like_count = response.xpath('count(//div[contains(@class, "_3gwk6") and contains(@class, "_nt9ow")]/a)').extract_first()
                liked_by = response.xpath('//div[contains(@class, "_3gwk6") and contains(@class, "_nt9ow")]/a/@href').extract()
        elif video_content:
            like_count = response.xpath('//div[@class="_m10kk"]/span/text()').extract_first()
            liked_by = []
        else:
            like_count = 0
            liked_by = []

        url = str(response.url).replace('/liked_by/', '')
        like_count = str(like_count).replace('.0', '')

        if int(like_count) != len(liked_by) and not video_content:
            item['unfinished_urls'].append(url)

        # Basic Post
        label = response.xpath('//img/@alt').extract()
        date = response.xpath('//a[@class="_djdmk"]/time/@datetime').extract_first()

        # # and @ from the label
        tags = response.xpath('//ul[@class="_b0tqa"]/li[1]/span/a[contains(@href, "tags")]/text()').extract()
        people_tagged = response.xpath('//ul[@class="_b0tqa"]/li[1]/span/a[@class="notranslate"]/text()').extract()

        # Instagram comment
        i = 1
        comments = list()
        person_commenting = 'start'
        while person_commenting is not None:
            person_commenting = response.xpath('//li[@class="_ezgzd"][%s]/a/text()' % i).extract_first()
            comment = response.xpath('//li[@class="_ezgzd"][%s]/span//text()' % i).extract()
            comments.append((person_commenting, comment))
            i += 1
        del comments[-1]

        item['posts'].append({
            'image_content': image_content,
            'video_content': video_content,
            'url': url,
            'label': label,
            'date': str(date).replace(':', '-'),
            'tags': tags,
            'image_tags': image_tags,
            'people_tagged': people_tagged,
            'like_count': like_count,
            'liked_by': liked_by,
            'comments': comments
        })

        yield item
