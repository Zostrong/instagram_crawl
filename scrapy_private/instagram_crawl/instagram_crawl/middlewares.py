# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/spider-middleware.html
import time
from scrapy.http import HtmlResponse
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, WebDriverException

# TODO: handle error while scrolling


class ChromeMiddleware(object):

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            iterate=crawler.spider.iterate,
        )

    def __init__(self, iterate):
        self.iterate = iterate
        self.start_url = ''
        self.picture_count = 0
        self.driver = webdriver.Chrome()

        self.driver.get('https://instagram.com')
        continue_to_login = self.driver.find_element_by_xpath('//p[@class="_g9ean"]/a')
        continue_to_login.click()

        time.sleep(1)
        print('Username:')
        username = input()
        print('Password:')
        password = input()
        # username = ''
        # password = ''

        # p = getpass.getpass()
        form_username = self.driver.find_element_by_xpath('//input[@type="text"]')
        form_password = self.driver.find_element_by_xpath('//input[@type="password"]')
        submit_button = self.driver.find_element_by_xpath('//form[@class="_3jvtb"]//button')

        form_username.send_keys(username)
        form_password.send_keys(password)
        submit_button.click()

        time.sleep(1)

    def process_request(self, request, spider):
        new = request.meta.get('new', 0)

        if new == 1 and not self.iterate:
            time.sleep(0.5)
            if not self.start_url:
                self.start_url = request.url
                self.driver.get(self.start_url)

            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(1)

            body = self.driver.page_source

            if not str(request.url).__contains__('.jpg') and not str(request.url).__contains__('.mp4'):
                return HtmlResponse(self.driver.current_url, body=body, encoding='utf-8', request=request)

        else:
            self.driver.get(request.url)

            continue_comment = "start"
            while continue_comment:
                try:
                    continue_comment = self.driver.find_element_by_xpath(
                        '//a[contains(@class, "_m3m1c") and contains(@class, "_1s3cd")]')
                    continue_comment.click()
                except NoSuchElementException:
                    continue_comment = None
                except WebDriverException:
                    pass
                time.sleep(0.5)

            try:
                like_list = self.driver.find_element_by_xpath('//a[@class="_nzn1h"]')
                like_count = int(self.driver.find_element_by_xpath('//a[@class="_nzn1h"]/span').text)

                like_list.click()
                time.sleep(0.5)

                like_div = self.driver.find_element_by_xpath('//div[contains(@class, "_ms7sh") '
                                                             'and contains(@class, "_2txtt")]')

                for i in range(0, like_count // 10):
                    self.driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", like_div)
                    time.sleep(1)
            except NoSuchElementException:
                pass
            except WebDriverException:
                pass

            try:
                video_like_list = self.driver.find_element_by_xpath('//span[@class="_m5zti"]')
                video_like_list.click()
                time.sleep(0.5)
            except NoSuchElementException:
                pass
            except WebDriverException:
                pass

            time.sleep(0.5)

            body = self.driver.page_source
            if not str(request.url).__contains__('.jpg') and not str(request.url).__contains__('.mp4'):
                return HtmlResponse(self.driver.current_url, body=body, encoding='utf-8', request=request)
