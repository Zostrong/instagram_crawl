# -*- coding: utf-8 -*-

# Scrapy settings for instagram_crawl project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'instagram_crawl'
ROBOTSTXT_OBEY = False

SPIDER_MODULES = ['instagram_crawl.spiders']
NEWSPIDER_MODULE = 'instagram_crawl.spiders'
DOWNLOAD_DELAY = 0.5
CONCURRENT_REQUESTS = 2


ITEM_PIPELINES = {
    'instagram_crawl.pipelines.MongoDBPipeline': 1,
    'instagram_crawl.pipelines.MyImagesPipeline': 2,
    'instagram_crawl.pipelines.MyFilesPipeline': 3,
}


MONGO_URI = 'mongodb://localhost:27017'
MONGO_DATABASE = 'instagram'
IMAGES_STORE = "pictures"
FILES_STORE = "pictures"


DOWNLOADER_MIDDLEWARES = {
   'instagram_crawl.middlewares.ChromeMiddleware': 543,
}
