# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class InstagramItem(scrapy.Item):

    # General Information
    _id = scrapy.Field()
    posts = scrapy.Field()
    urls = scrapy.Field()
    unfinished_urls = scrapy.Field()

    # Image Information for scrapy
    image_urls = scrapy.Field()
    images = scrapy.Field()
    file_urls = scrapy.Field()
    files = scrapy.Field()
