# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from urllib.request import urlopen

import pymongo as pymongo
from scrapy.pipelines.files import FilesPipeline

from scrapy import Request
from scrapy.pipelines.images import ImagesPipeline


class MongoDBPipeline(object):
    collection_name = 'instagram_initial'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        # self.iteration = iteration

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE'),
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.iteration = spider.iterate

    def process_item(self, item, spider):
        # Save every data
        if self.iteration is False:
            db_item = {
                '_id': item['_id'],
                'posts': item['posts'],
                'unfinished_urls': item['unfinished_urls']
            }
            self.db[self.collection_name].update({'_id': item['_id']}, db_item, upsert=True)
        else:
            url = str(item['posts'][-1]['url'])
            liked_by = item['posts'][-1]['liked_by']
            like_count = int(item['posts'][-1]['like_count'])
            video_content = item['posts'][-1].get('video_content', '')
            self.db[self.collection_name].update({'_id': item['_id'], 'posts.url': url}, {'$set': {'posts.$.liked_by': liked_by}})

            if (like_count == len(liked_by)) or video_content:
                self.db[self.collection_name].update({'_id': item['_id']}, {'$pull': {'unfinished_urls': url}})

        return item


class MyImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        date = next((post['date'] for post in item['posts'] if post['image_content'] == item['image_urls'] is not None))
        meta = {
            'image_name': str(date),
            'image_path': str(item['_id'])
        }
        return [Request(image, meta=meta)
                for image in item.get('image_urls', [])]

    def file_path(self, request, response=None, info=None):
        return '%s/%s.jpg' % (request.meta['image_path'], request.meta['image_name'])


class MyFilesPipeline(FilesPipeline):

    def get_media_requests(self, item, info):
        date = next((post['date'] for post in item['posts'] if post['video_content'] == item['file_urls'] is not None))
        file_name = 'pictures/%s/%s.mp4' % (item['_id'], str(date))
        if item['file_urls']:
            html = urlopen(item['file_urls'][0])
            with open(file_name, 'wb') as f:
                f.write(html.read())
