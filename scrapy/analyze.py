from collections import defaultdict
from sys import argv
from time import sleep

import matplotlib.pyplot
from cassandra.cluster import Cluster
from cassandra.query import dict_factory

from scrapy.custom_enum import Period, Feature
from scrapy.instagram_data import Instagram_data


def connect_to_cassandra():
    cluster = Cluster()
    session = cluster.connect('instagram')
    return session


def create_index(session, column):
    sql = "CREATE INDEX IF NOT EXISTS ON scrapy_items(%s)" % column
    session.execute(sql)
    sleep(0.5)  #cassandra needs time to precces the new index


def select_items(session, is_list, column, value):
    result = list()
    session.row_factory = dict_factory

    if is_list is True:
        sql = "SELECT * FROM scrapy_items WHERE %s CONTAINS '%s';" % (column, value)
    else:
        sql = "SELECT * FROM scrapy_items WHERE %s='%s';" % (column, value)

    items = session.execute(sql)
    for item in items:
        result.append(Instagram_data(item['id'], item['name'], item['tags'], item['people'], item['likes'], item['dates']))
    return result


def create_analytics(instagram_items, period, feature):
    date = ''
    graph_pre_data = defaultdict(lambda: 0)

    for item in instagram_items:

        year, month, day = item.date.split('-')
        if period == Period.DAY:
            date = year + '-' + month + '-' + day
        if period == Period.MONTH:
            date = year + '-' + month
        if period == Period.YEAR:
            date = year

        if feature == Feature.LIKES:
            graph_pre_data[date] += int(str(item.likes).replace(',', ''))

        if feature == Feature.TAGS:
            if item.tags != 0:
                graph_pre_data[date] += len(item.tags)
            else:
                graph_pre_data[date] += 0
        if feature == Feature.PEOPLE:
            if item.people != 0:
                graph_pre_data[date] += len(item.people)
            else:
                graph_pre_data[date] += 0
    return graph_pre_data


def create_graph(grap_pre_data, filename):
    data_list_tuple = sorted(grap_pre_data.items())
    date, value = zip(*data_list_tuple)
    value_range = max(value)

    data_increment = len(date) // 10
    if value_range > 10:
        value_increment = value_range // 10
    else:
        value_increment = 1
    date_list = date[::data_increment]

    value_list = list()
    y_value = 0
    for data in date_list:
        y_value += value_increment
        value_list.append(y_value)

    plt_like = matplotlib.pyplot
    plt_like.bar(date, value, color='b')
    plt_like.yticks(range(0, value_range, value_increment), value_list)
    plt_like.xticks(range(0, len(date), data_increment), date_list, rotation=90)
    plt_like.savefig(filename + '.png')


def create_csv(datas):
    with open('text.txt', 'w') as csv:
        datas.sort(key=lambda data: data.date)
        for data in datas:
            csv.write(str(data.date) + '\t' + str(data.likes) + '\n')


def main(argv):
    column_value = argv[1]
    column_request = argv[2]
    filename = argv[4]
    is_list = column_request == 'tags' or column_request == 'people'

    period = ''
    if argv[3] == 'day':
        period = Period.DAY
    elif argv[3] == 'month':
        period = Period.MONTH
    elif argv[3] == 'year':
        period = Period.YEAR

    feature = ''
    if argv[5] == 'likes':
        feature = Feature.LIKES
    elif argv[5] == 'tags':
        feature = Feature.TAGS
    elif argv[5] == 'people':
        feature = Feature.PEOPLE

    session = connect_to_cassandra()
    create_index(session, column_request)
    result = select_items(session, is_list, column_request, column_value)

    graph_pre_data = create_analytics(result, period, feature)
    create_graph(graph_pre_data, filename)
    create_csv(result)


if __name__ == '__main__':
    main(argv)
