"""
Script for creating the meta data next to the scrapped pictures

RUN AFTER SCRAPY FINISHED
"""


import json
from collections import Counter

import jsonlines
comments = list()
count = 0
with jsonlines.open('result.jl') as reader:
    for item in reader.iter(type=dict, skip_invalid=True):
        json_filename = 'pictures/%s/%s.json' % (str(item['name']), str(item['date']).replace(':', '-'))
        with open(json_filename, 'w', encoding='utf8') as json_writer:
            json.dump(dict(item), json_writer, indent=4, ensure_ascii=False)
        for users in item['comment']['name']:
            for user in users['name']:
                print(user['comments'])
                if item['name'] == 'banfiblanka':
                    comments.append(user)
print(Counter(comments))
print(count)
