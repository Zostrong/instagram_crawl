# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class InstagramItem(scrapy.Item):

    # General Information
    name = scrapy.Field()
    # picture_url = scrapy.Field()
    tags = scrapy.Field()
    people = scrapy.Field()
    likes = scrapy.Field()
    date = scrapy.Field()

    # Image information for users
    label = scrapy.Field()
    image_tags = scrapy.Field()

    # CommentItem container
    comment = scrapy.Field()

    # Image Information for scrapy
    image_urls = scrapy.Field()
    images = scrapy.Field()
    file_urls = scrapy.Field()
    files = scrapy.Field()


class CommentItem(scrapy.Item):
    name = scrapy.Field()
    comment = scrapy.Field()
