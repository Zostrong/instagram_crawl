import scrapy
from scrapy.exceptions import CloseSpider
from instagram_crawl.items import InstagramItem
from instagram_crawl.items import CommentItem


class QuotesSpider(scrapy.Spider):
    name = 'instagram'
    person = ''
    hashtag = ''
    page_scanned = 0
    pictures = 0
    max_pictures = 0

    def start_requests(self):
        url = 'https://www.instagram.com/'

        self.hashtag = getattr(self, 'hashtag', '')
        self.person = getattr(self, 'person', '')
        try:
            self.max_pictures = int(getattr(self, 'max', 12))
        except Exception:
            raise CloseSpider(reason="Max value has to be a number")

        if self.person == '' and self.hashtag == '':
            raise CloseSpider('reason=Person or Hashtag has to be given')
        if self.person != '' and self.hashtag == '':
                url = url + self.person + '/'
        if self.hashtag != '' and self.person == '':
            url = url + 'explore/tags/' + self.hashtag + '/'

        yield scrapy.Request(url, self.parse)

    def parse(self, response):
        for href in response.css('div._cmdpi a'):
            if self.pictures < self.max_pictures:
                self.pictures = self.pictures + 1
                yield response.follow(href, self.parse_post)

        for href in response.xpath('//a[contains(@class, "_1cr2e") and contains(@class, "_epyes")]'):
            if self.page_scanned < (self.max_pictures // 12):
                self.page_scanned = self.page_scanned + 1
                yield response.follow(href, self.parse)

    def parse_post(self, response):

        # Instagram Item
        item = InstagramItem()
        # Basic Post
        item['name'] = response.xpath('//div[@class="_eeohz"]/a/text()').extract_first()
        item['label'] = response.xpath('//img/@alt').extract()
        item['date'] = response.xpath('//a[@class="_djdmk"]/time/@datetime').extract_first()

        # # and @ from the label
        item['tags'] = response.xpath('//ul[@class="_b0tqa"]/li[1]/span/a[contains(@href, "tags")]/text()').extract()
        item['people'] = response.xpath('//ul[@class="_b0tqa"]/li[1]/span/a[@class="notranslate"]/text()').extract()

        # Likes
        likes = response.xpath('//div[contains(@class, "_3gwk6")]/span/span/text()').extract_first()
        # likes = response.xpath('//span[@class="_nzn1h"]/span/text()').extract_first()
        if likes is not None:
            item['likes'] = likes
        else:
            item['likes'] = response.xpath('count(//div[contains(@class, "_3gwk6") and contains(@class, "_nt9ow")]/a)').extract_first()

        # Information of the image
        item['image_urls'] = []
        item['file_urls'] = []
        item['image_tags'] = response.xpath('//a[contains(@class, "_n1lhu") and contains(@class, "_4dsc8")]/@href').extract()
        image_content = response.xpath('//img[@class="_2di5p"]/@src').extract()
        video_content = response.xpath('//video[@class="_l6uaz"]/@src').extract()

        if image_content is not None:
            item['image_urls'] = image_content
        if video_content is not None:
            item['file_urls'] = video_content

        # Instagram comment
        comment_item = CommentItem()
        comment_item['name'] = response.xpath('//li[@class="_ezgzd"]/a/text()').extract()
        comment_item['comment'] = []

        comments = response.xpath('//li[@class="_ezgzd"]/span//text()').extract()
        number_of_comments = len(comment_item['name'])
        number_of_child_element = []

        for i in range(1, number_of_comments):
            number_of_lines = response.xpath('count(//li[@class="_ezgzd"][%s]/span//text())' % i).extract_first()
            number_of_child_element.append(number_of_lines)

        start = 0
        for elements in number_of_child_element:
            end = start + int(str(elements).split('.')[0])
            comment_item['comment'].append(comments[start:end])
            start = end

        item['comment'] = comment_item

        yield item
