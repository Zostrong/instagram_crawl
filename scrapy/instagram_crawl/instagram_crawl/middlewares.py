# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/spider-middleware.html
import time
from scrapy.http import HtmlResponse
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, WebDriverException


class ChromeMiddleware(object):

    def __init__(self):
        self.driver = webdriver.Chrome()
        # self.driver.get('https://instagram.com')
        # continue_to_login = self.driver.find_element_by_xpath('//p[@class="_g9ean"]/a')
        # continue_to_login.click()
        # time.sleep(1)
        #
        # print('Username:')
        # username = input()
        # print('Password:')
        # password = input()
        #
        # # p = getpass.getpass()
        # form_username = self.driver.find_element_by_xpath('//input[@type="text"]')
        # form_password = self.driver.find_element_by_xpath('//input[@type="password"]')
        # submit_button = self.driver.find_element_by_xpath('//form[@class="_3jvtb"]//button')
        #
        # form_username.send_keys(username)
        # form_password.send_keys(password)
        # submit_button.click()
        #
        # time.sleep(1)

    def process_request(self, request, spider):

        self.driver.get(request.url)
        continue_comment = "start"
        while continue_comment:
            try:
                continue_comment = self.driver.find_element_by_xpath(
                    '//a[contains(@class, "_m3m1c") and contains(@class, "_1s3cd")]')
                continue_comment.click()
            except NoSuchElementException:
                continue_comment = None
            except WebDriverException:
                pass
            time.sleep(0.5)

        # TODO: Miért??
        body = self.driver.page_source
        if not str(request.url).__contains__('.jpg') and not str(request.url).__contains__('.mp4'):
            return HtmlResponse(self.driver.current_url, body=body, encoding='utf-8', request=request)

