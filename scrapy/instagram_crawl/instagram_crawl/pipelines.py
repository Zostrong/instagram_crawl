# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
import uuid
from urllib.request import urlopen

import pymongo as pymongo
from cassandra.cluster import Cluster
from scrapy.pipelines.files import FilesPipeline

from scrapy import Request
from scrapy.pipelines.images import ImagesPipeline


class CassandraPipeline(object):

    def __init__(self):
        self.cassandra_keyspace = 'instagram'

    def open_spider(self, spider):
        cluster = Cluster()
        self.session = cluster.connect(self.cassandra_keyspace)
        self.session.execute("CREATE TABLE IF NOT EXISTS " + self.cassandra_keyspace + ".scrapy_items ( id uuid, name text, tags list<text>, people list<text>, likes text, dates text,  PRIMARY KEY (id))")

    def process_item(self, item, spider):
        name = str(item['name'])
        tags = list()
        for tag in item['tags']:
            tags.append(str(tag).replace('#', ''))

        people = list()
        for person in item['people']:
            people.append(str(person).replace('@', ''))

        likes = str(item['likes']).replace(" ", "")
        dates = str(item['date']).split("T")[0]

        sql = "INSERT INTO scrapy_items (id, name, tags, people, likes, dates) VALUES (%s, %s, %s, %s, %s, %s)"
        self.session.execute(sql, parameters=(uuid.uuid1(), name, tags, people, likes, dates))
        return item


class MongoDBPipeline(object):
    collection_name = 'instagram_initial'
    collection_name_users = 'insta_users'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def process_item(self, item, spider):
        # Save every data
        self.db[self.collection_name].insert(dict(item))

        #Save users from comments
        users = {
            'name': item['comment']['name'],
            'useful': 1
        }
        # TODO: add @person (with class 'notranslate' from comments
        self.db[self.collection_name_users].insert(users)

        return item


class MyImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        meta = {
            'image_name': str(item['date']).replace(":", "-"),
            'image_path': str(item['name'])
        }

        return [Request(image, meta=meta)
                for image in item.get('image_urls', [])]

    def file_path(self, request, response=None, info=None):
        return '%s/%s.jpg' % (request.meta['image_path'], request.meta['image_name'])


class MyFilesPipeline(FilesPipeline):

    def get_media_requests(self, item, info):
        file_name = 'pictures/%s/%s.mp4' % (item['name'], str(item['date']).replace(":", "-"))
        if item['file_urls']:
            html = urlopen(item['file_urls'][0])
            with open(file_name, 'wb') as f:
                f.write(html.read())

