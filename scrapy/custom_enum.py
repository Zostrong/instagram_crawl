from enum import Enum


class Period(Enum):
    DAY = 'day'
    MONTH = 'month'
    YEAR = 'year'


class Feature(Enum):
    LIKES = 'likes'
    TAGS = 'tags'
    PEOPLE = 'people'
