class Instagram_data(object):
    def __init__(self, uuid, name, tags, people, likes, date):
        self.uuid = uuid
        self.name = name
        self.date = date
        self.likes = str(likes).split('.')[0]

        if tags is None:
            self.tags = 0
        else:
            self.tags = tags

        if people is None:
            self.people = 0
        else:
            self.people = people
